from django.contrib import admin
from patient.models import Patient, Gender, MaritalStatus, Name, BloodGroup

# Register your models here.
admin.site.register(Patient)
admin.site.register(Gender)
admin.site.register(MaritalStatus)
admin.site.register(Name)
admin.site.register(BloodGroup)