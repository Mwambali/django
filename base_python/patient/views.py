from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import permissions
from patient.serializers import (NameSerializer, GenderSerializer, MaritalStatusSerializer,
    BloodGroupSerializer, PatientSerializer)
from patient.models  import Name, Patient, BloodGroup,MaritalStatus, Gender
from rest_framework.decorators import api_view

# Create your views here.

class NameViewSet(viewsets.ModelViewSet):
    queryset = Name.objects.all()
    serializer_class = NameSerializer
    permissions_classes = [permissions.IsAuthenticated]

class GenderViewSet(viewsets.ModelViewSet):
    queryset = Gender.objects.all()
    serializer_class = GenderSerializer
    permissions_classes = [permissions.IsAuthenticated]

class MaritalStatusViewSet(viewsets.ModelViewSet):
    queryset = MaritalStatus.objects.all()
    serializers_class = MaritalStatusSerializer
    permissions_classes =[permissions.IsAuthenticated]

class BloodGroupViewSet(viewsets.ModelViewSet):
    queryset  = BloodGroup.objects.all()
    serializers_class = BloodGroupSerializer
    permissions_classes = [permissions.IsAuthenticated]

class PatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all()
    serializers_class  = PatientSerializer
    permissions_classes = [permissions.IsAuthenticated]

    @api_view(['POST',])
    def create_patient(request):
        if request.method == 'POST':
            serializer = PatientSerializer(data = request.data)
            data = {}

            if serializer.is_valid():
                patient = serializer.save()
                data['message'] = "patient added successfully"
                data['PatientName'] = Patient.PatientName
                data['Gender']  = Patient.Gender
                data['MaritalStatus'] = Patient.MaritalStatus
                data['BloodGroup'] = Patient.BloodGroup
                data['Birthday']  = Patient.Birthday
            else:
                data = serializer.errors

            return Response(data)


