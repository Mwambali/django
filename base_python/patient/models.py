from django.db import models

# Create your models here.
class Name(models.Model):
    text   = models.CharField(max_length= 30, blank = False, null = False)
    given  = models.CharField(max_length=30, blank = False, null = False)
    family = models.CharField(max_length=30, blank = False, null = False)
    use    = models.CharField(max_length= 30, blank =False, null = False)

    def __str__(self):
        return "{}" .format(self.family)

class Gender(models.Model):

    gender = (
        ('M', ('Male')),
        ('F', ('Female')),
        ('Both',('Both')),
        ('Nan',('None')),
    )

    code = models.CharField(max_length = 10, null = False, choices = gender)

    def __str__(self):
        return "{}".format(self.code)

class BloodGroup(models.Model):
    bloodgroup =(
        ('A+',('A+')),
        ('A',('A-')),
        ('B',('B+')),
        ('B',('B-')),
        ('AB+',('AB+')),
        ('AB-',('AB-')),
        ('0+',('0+')),
        ('0-',('0-')),

    )
    display  = models.CharField(max_length =5, null = False, choices = bloodgroup)

    def __str__(self):
        return "{}".format(self.display)

class MaritalStatus(models.Model):
    maritalStatus = (
        ('Single',('Single')),
        ('Married',('Married')),
        ('Window(er)',('Window(er)')),
        ('Other',('Other')),
    )

    display = models.CharField(max_length = 13, choices = maritalStatus)

    def  __str__(self):
        return "{}".format(self.display)


class Patient(models.Model):

    PatientName   = models.ForeignKey(Name, on_delete = models.CASCADE),
    Gender        = models.ForeignKey(Gender, on_delete = models.CASCADE),
    BloodGroup    = models.ForeignKey(BloodGroup, on_delete = models.CASCADE),
    MaritalStatus = models.ForeignKey(MaritalStatus, on_delete = models.CASCADE),
    Birthday      = models.DateField()

    def __str__(self):
        return "{}".format(self,PatientName)