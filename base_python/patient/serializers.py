from rest_framework import serializers
from patient.models import Patient, Name, Gender, BloodGroup, MaritalStatus


class NameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Name
        fields = ['url','text','given','family','use']

class GenderSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Gender
        fields =['url','code']


class BloodGroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BloodGroup
        fields = ['url', 'display']

class MaritalStatusSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MaritalStatus
        fields = ['url','display']

class PatientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Patient
        fields = ['url', 'PatientName','Gender','MaritalStatus','BloodGroup','Birthday']
        def save(self):
            patient = Patient(
                PatientName = self.validated_data['PatientName'],
                Gender      = self.validated_data['Gender'],
                MaritalStatus = self.validated_data['MaritalStatus'],
                BloodGroup    = self.validated_data['BloodGroup'],
                Birthday       = self.validated_data['Birthday'],

            )

            patient.save()
            return patient