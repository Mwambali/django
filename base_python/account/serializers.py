from rest_framework import serializers 
from account.models import User

#to register a new user 
class UserSerializer(serializers.HyperlinkedModelSerializer):
    password2 = serializers.CharField(style={'input_type':'password'},write_only=True)  
    class Meta:
        model = User
        fields = ['url','email','username','password', 'password2']
        extra ={
              'password':{'write_only' : True}
        }

    def save(self):

        user = User(
            email = self.validated_data['email'],
            username = self.validated_data['username'],
            )
        password = self.validated_data['password'],
        password2 = self.validated_data['password2'],

        if password != password2:
            raise serializers.ValidationError({'password:Password must match'})
        user.set_password(password)
        user.save()

        return user
