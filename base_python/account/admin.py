from django.contrib import admin
from account.models import User

# Register your models here.


#custom user
class UserAdmin(admin.ModelAdmin):
    search_fields = ["email"]
    class Meta:
        model = User
admin.site.register(User, UserAdmin)