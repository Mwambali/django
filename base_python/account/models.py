from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
# Create your models here.


class AccountManager(BaseUserManager):
    def create_user(self, email, username, password=None):
        if not email:
            raise ValueError("email must be provided")
        if not username:
            raise ValueError("username must be provided")
        if not password:
            raise ValueError("password must be provided")

        user = self.model(
            email = self.normalize_email(email),
            username = username
        )
        user.set_password(password)
        user.save(using = self._db)
        return user

    def create_staff(self, email,username, password=None):
        user = self.create_user(
            email = self.normalize_email(email),
            username = username,
            password = password,
        )

        user.is_admin = False
        user.is_superuser = False
        user.is_staff = True
        user.is_active = True
        user.save(using = self._db)
        return user

    def create_superuser(self, email, username, password= None):
        user = self.create_user(
            email = self.normalize_email(email),
            username = username,
            password = password,
        )
        user.is_admin     = True
        user.is_superuser = True
        user.is_staff     = True
        user.is_superuser = True
        user.save(using = self._db)
        return  user

class User(AbstractBaseUser):

    email           = models.EmailField(verbose_name = "email", max_length = 255,unique=True,)
    username        = models.CharField(verbose_name = "username", max_length = 60, blank=False,)
    date_joined     = models.DateTimeField(verbose_name ="date_joined", auto_now_add=True)
    last_login      = models.DateTimeField(auto_now=True)
    is_active       = models.BooleanField(default=True)
    is_staff        = models.BooleanField(default=False) # a admin user; non super-user
    is_admin        = models.BooleanField(default=False) # a superuse
    is_superuser    = models.BooleanField(default=False)
  

    #specify what we want the user to login with and field required when registering
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username',] #many can be spcify

    #set userAccout to use AccountManager
    objects = AccountManager()

    #display email
    def __str__(self):
        return self.email 

    #all permission to the user
    def has_perm(self, perm, obj = None):
        return True
    #permission to view all the app

    def has_module_perms(self, app_label):
        return True

      