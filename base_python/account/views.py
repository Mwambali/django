from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import permissions
from account.serializers import UserSerializer
from account.models  import User
from patient.models import Patient
from patient.serializers import PatientSerializer
from rest_framework.decorators import api_view

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects
    serializer_class = UserSerializer
    permissions_classes = [permissions.IsAuthenticated]

    @api_view(['POST',])
    def create_user(request):
        if request.method == 'POST':
            serializer = UserSerializer(data = request.data)
            data = {}
            if serializer.is_valid():
                user = serializer.save()
                data['message'] ="user added succesfully"
                data['email']  = UserAccount.email
                data['username'] = UserAccount.username
            else:
                data = serializer.errors
            return Response(data)

    @api_view(['GET',])
    def create_patient(request, slug):
        try:
            patient = Patient.objects.get(slug = slug)
        except Patient.DoesNotExist:
            return Response(status = status.HTTP_404_NOT_FOUND)

        if resquest.method == 'GET':
            serializer = PatientSerializer(patient)
            return Response(serializer.data)

    @api_view(['PUT',])
    def update_patient(request, slug):
        try:
            patient = Patient.objects.get(slug = slug)
        except Patient.DoesNotExist:
            return Response(status = status.HTTP_404_NOT_FOUND)

        if resquest.method == 'PUT':
            serializer = PatientSerializer(patient, data = resquest.data)
            data = {}

            if serializer.is_valid():
                serializer.save()
                data["message"] = "patient update succesfully"
                return Response(data = data)
            return Response(serializer.errors, status = status.HTTP_404_NOT_FOUND)

    @api_view(['DELETE',])
    def delete_patient(request, slug):
        try:
            patient = Patient.objects.get(slug = slug)
        except  Patient.DoesNotExist:
            return Response(status = status.HTTP_404_NOT_FOUND)

        if resquest.method == 'DELETE':
            do = patient.delete()
            data = {}
            if do:
                data["message"] ="patient delete succesfully"
            else:
                data["failure"] =" delet failde"
            return Response(data=data)





